<?
class PluginK2Import_ModuleK2import_MapperK2Import extends Mapper
{

    public function getUserList($prefix)
    {
	$sql = "SELECT 
		k2u.id,
		k2u.username,
	    ju.username as 'login',
		ju.name, 
		ju.email,
		k2u.image,
		k2u.url
		FROM ".$prefix."_k2_users k2u 
			left outer join ".$prefix."_users ju on ju.id = k2u.userid ";
	$aReturn = array();
	if ($aRows = $this->oDb->select($sql)) {
	    foreach ($aRows as $aRow) {
		$aReturn[$aRow['id']] = $aRow;
	    }
	    return $aReturn;
	}

	return false;
    }

    public function getUser($prefix,$uid)
    {
	$sql = "SELECT 
		k2u.id,
		k2u.username,
		k2u.url,
	    ju.username as 'login',
		ju.name, 
		ju.email,
		k2u.gender,
		k2u.image,
		k2u.url,
		ju.registerDate,
		ju.lastvisitDate,
		(SELECT COUNT(*) FROM ".$prefix."_k2_items WHERE created_by = k2u.userId) as 'count'
		FROM ".$prefix."_k2_users k2u 
			left outer join ".$prefix."_users ju on ju.id = k2u.userid
		where k2u.id = ".intval($uid);

	if ($aRows = $this->oDb->select($sql)) {
	    foreach ($aRows as $aRow) {
			$aReturn[$aRow['id']] = $aRow;
	    }
	    return $aReturn;
	}

	return false;
    }

    public function getCat($prefix,$cid) {
    	$sql = "SELECT id,name,alias,description FROM ".$prefix."_k2_categories where id=".intval($cid);
		if ($aRows = $this->oDb->select($sql)) {
		    foreach ($aRows as $aRow) {
				$aReturn[$aRow['id']] = $aRow;
				$aReturn[$aRow['id']]['alias'] = htmlspecialchars($aReturn[$aRow['id']]['alias']);
				$aReturn[$aRow['id']]['name'] = htmlspecialchars($aReturn[$aRow['id']]['name']);
				$aReturn[$aRow['id']]['description'] = htmlspecialchars($aReturn[$aRow['id']]['description']);				
		    }
		    return $aReturn;
		}

		return false;

    }
    public function getCatList($prefix) {
    	$sql = "SELECT id,name,alias,description FROM ".$prefix."_k2_categories";
		if ($aRows = $this->oDb->select($sql)) {
		    foreach ($aRows as $aRow) {
				$aReturn[$aRow['id']] = $aRow;
				$aReturn[$aRow['id']]['alias'] = htmlspecialchars($aReturn[$aRow['id']]['alias']);
				$aReturn[$aRow['id']]['name'] = htmlspecialchars($aReturn[$aRow['id']]['name']);
				$aReturn[$aRow['id']]['description'] = htmlspecialchars($aReturn[$aRow['id']]['description']);				
		    }
		    return $aReturn;
		}

		return false;

    }

    public function getTopicCount($prefix) {
    	$sql = "SELECT count(*) as 'count' FROM ".$prefix."_k2_items";
    	if($aRow = $this->oDb->select($sql)) {    		
    		return $aRow[0]['count'];
    	}
    	return 1;
    }

    public function getTopicList($prefix,$page,$pagesize) {
    	$sql = "SELECT 
    		i.id,
    		i.title,
    		coalesce(ju.username,'administrator') as 'created_by',
    		i.introtext as 'hash' 
    	FROM ".$prefix."_k2_items i 
			left outer join ".$prefix."_users ju on ju.id = i.created_by    	
    	order by id desc";
    	if(isset($page) && isset($pagesize)) {
    		$sql .= ' limit '.intval($page-1)*intval($pagesize).",".intval($pagesize);
//    		print $sql;
//    		die;
    	}
		if ($aRows = $this->oDb->select($sql)) {
		    foreach ($aRows as $aRow) {
				$aReturn[$aRow['id']] = $aRow;
				$aReturn[$aRow['id']]['title'] = htmlspecialchars($aReturn[$aRow['id']]['title']);
		    }
		    $count = $this->getTopicCount($prefix);
		    return array('count'=>$count,'collection'=>$aReturn);
		}

		return false;
    }

    public function getComments($prefix,$cid) {
    	$sql = "
    		SELECT 
    			jc.id,
    			ju.username as 'login',
    			jc.parent,
    			jc.userid,
    			jc.name,
    			jc.email,
    			jc.ip,
    			jc.date,
    			jc.comment 
    		FROM `".$prefix."_jcomments` jc
 				left outer join ".$prefix."_users ju on  ju.id = jc.userid
     			WHERE `object_group` = 'com_k2' and `object_id` = ".intval($cid);
		if ($aRows = $this->oDb->select($sql)) {
		    foreach ($aRows as $aRow) {
				$aReturn[$aRow['parent']][$aRow['id']] = $aRow;
		    }
		    return $aReturn;
		}

		return false;
    }

    public function getTopic($prefix,$tid) {
    	$sql = "SELECT 
					 i.id,
					 i.title,
					 cat.name as 'cat',
					 i.created as 'date',
					 i.introtext,
					 i.fulltext,
					 i.gallery, 
					 i.video,
					 coalesce(group_concat(tags.name separator ','),'') as 'tags',
					 ju.username as 'created_by'
				FROM ".$prefix."_k2_items i
					  left outer join ".$prefix."_k2_categories cat on cat.id = i.catid
					  left outer join ".$prefix."_k2_tags_xref tags_xref on tags_xref.itemID = i.id
					  left outer join ".$prefix."_k2_tags tags on tags.id = tags_xref.tagId
				left outer join ".$prefix."_users ju on ju.id = i.created_by
				where i.id = ".intval($tid)."
				group by i.id ";
		if ($aRows = $this->oDb->select($sql)) {
		    foreach ($aRows as $aRow) {
				$aReturn[$aRow['id']] = $aRow;
				$aReturn[$aRow['id']]['title'] = htmlspecialchars($aReturn[$aRow['id']]['title']);
		    }
		    return $aReturn;
		}

		return false;
    }

}
?>