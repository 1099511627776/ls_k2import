<?php

$config = array();

$config['joomladb'] = 
	array(
		'type'=>'mysql',
		'user'=>'',
		'pass'=>'',
		'host'=>'localhost',
		'port'=>'3306',
		'dbname'=>''
		);

$config['joomla_prefix'] = '';
$config['joomlasite'] = '';
$config['joomla_fileroot'] = '';
$config['joomla_galleryroot'] = 'media/k2/galleries/';
$config['joomla_avatars'] = 'media/k2/users/';

Config::Set('router.page.k2import', 'PluginK2import_ActionAdmin');
return $config;
?>
